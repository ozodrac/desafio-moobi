-- --------------------------------------------------------
-- Servidor:                     localhost
-- Versão do servidor:           10.2.6-MariaDB-log - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para desafio-moobi
CREATE DATABASE IF NOT EXISTS `desafio-moobi` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci */;
USE `desafio-moobi`;

-- Copiando estrutura para tabela desafio-moobi.clientes
CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Copiando dados para a tabela desafio-moobi.clientes: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT IGNORE INTO `clientes` (`id`, `nome`) VALUES
	(1, 'zezinho'),
	(2, 'huguinho'),
	(3, 'luizinho'),
	(4, 'Tio Patinhas');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;

-- Copiando estrutura para tabela desafio-moobi.pedidos
CREATE TABLE IF NOT EXISTS `pedidos` (
  `idpedido` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `parcelas` int(11) DEFAULT 1,
  `valor` decimal(10,2) NOT NULL,
  `forma_pagamento` set('CC','CD','V') COLLATE latin1_general_ci NOT NULL DEFAULT 'V',
  `ip` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`idpedido`),
  KEY `fk_pedidos_id_clientes_id_idx` (`id_cliente`),
  CONSTRAINT `fk_pedidos_id_clientes_id` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Copiando dados para a tabela desafio-moobi.pedidos: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `pedidos` DISABLE KEYS */;
INSERT IGNORE INTO `pedidos` (`idpedido`, `id_cliente`, `parcelas`, `valor`, `forma_pagamento`, `ip`) VALUES
	(1, 1, 4, 100.00, 'CC', NULL),
	(2, 2, 3, 320.53, 'CD', NULL),
	(3, 3, 1, 15.00, 'CD', NULL),
	(4, 4, 2, 1933.65, 'V', NULL),
	(5, 4, 4, 6750.31, 'V', NULL);
/*!40000 ALTER TABLE `pedidos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
