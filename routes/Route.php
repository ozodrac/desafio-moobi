<?php
namespace Routes;

use Config\Routes\Routers;

class Route extends Routers
{
    
    protected function init(){
        $routes['home'] = array('route'=>'/', 
                                'controller'=> 'Home', 
                                'action'=>'index'
                                );
        $routes[] = array('route'=>'/pedidos', 'controller'=>'Pedidos', 'action'=>'index');
        $routes[] = array('route'=>'/pedidos/', 'controller'=>'Pedidos', 'action'=>'index');
        $routes[] = array('route'=>'/pedidos/novo', 'controller'=>'Pedidos', 'action'=>'create');
        $routes[] = array('route'=>'/pedidos/save', 'controller'=>'Pedidos', 'action'=>'store');
        $routes[] = array('route'=>'/pedidos/:id', 'controller'=>'Pedidos', 'action'=>'edit');
        $routes[] = array('route'=>'/pedidos/delete/:id', 'controller'=>'Pedidos', 'action'=>'delete');

        $routes[] = array('route'=>'/clientes', 'controller'=>'Clientes', 'action'=>'index');
        $routes[] = array('route'=>'/clientes/', 'controller'=>'Clientes', 'action'=>'index');
        $routes[] = array('route'=>'/clientes/novo', 'controller'=>'Clientes', 'action'=>'create');
        $routes[] = array('route'=>'/clientes/save', 'controller'=>'Clientes', 'action'=>'store');

        
        $this->setRoutes($routes);
    }

   
}
