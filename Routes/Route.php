<?php
namespace Routes;

use Config\Routes\Routers;

class Route extends Routers
{
    
    protected function init(){
        $routes['home'] = array('route'=>'/', 
                                'controller'=>'indexController', 
                                'action'=>'index'
                                );
        $routes['pedidos'] = array('route'=>'/pedidos', 
                                'controller'=>'Pedidos', 
                                'action'=>'index'
                                );
        $routes['clientes'] = array('route'=>'/clientes', 'controller'=>'Clientes', 'action'=>'index');
        $routes[] = array('route'=>'/clientes/', 'controller'=>'Clientes', 'action'=>'index');
        $routes[] = array('route'=>'/clientes/novo', 'controller'=>'Clientes', 'action'=>'create');

        
        $this->setRoutes($routes);
    }
    public function getUrl()
    {
        return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    }
}
