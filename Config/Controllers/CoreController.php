<?php
namespace Config\Controllers;

abstract class CoreController
{
    protected $view;
    private $action;

    
    public function __construct()
    {
        $this->view = new \stdClass;
    }
    protected function render($action, $template=true)
    {
        $this->action = $action;
        if($template && file_exists("../App/Views/template.php")){
            include_once "../App/Views/template.php";
        }else{
            $this->content();
        }
    }

    protected function content()
    {
        $current = get_class($this);
        $singleClassName = strtolower((str_replace("Controller", '', str_replace("App\\Controllers\\", "", $current))));
        include_once "../App/Views/" . $singleClassName . "/" . $this->action . ".php";
    }

    protected function header()
    {
        include_once "../App/Views/header.php";
    }
    protected function footer()
    {
        include_once "../App/Views/footer.php";
    }
    protected function scripts()
    {
        include_once "../App/Views/scripts.php";
    }
    protected function sidebar()
    {
        include_once "../App/Views/sidebar.php";
    }
    protected function topbar()
    {
        include_once "../App/Views/topbar.php";
    }

    
}
