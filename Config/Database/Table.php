<?php

namespace Config\Database;

class Table
{
    protected $db;
    protected $table;
    protected $fields;
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }
    public function getAll()
    {
        $query = " SELECT * FROM {$this->table};"; //.$this->table;
        return $this->db->query($query)->fetchAll(\PDO::FETCH_OBJ);
    }
    public function find($id)
    {
        $query = " SELECT * FROM {$this->table} where id=:id;"; 
        $pdo = $this->db->prepare($query);
        $pdo->bindParam(":id",$id);
        $pdo->execute();
        return $pdo->fetch(\PDO::FETCH_ASSOC);
    }
    public function delete($id)
    {
        $query = " DELETE FROM {$this->table} where id=:id;"; 
        $pdo = $this->db->prepare($query);
        $pdo->bindParam(":id",$id);
        $pdo->execute();
        return $pdo->rowCount();
    }
    public function store(array $dados)
    {
        $campos = array();
        foreach ($dados as $campo =>$value){
            array_push($campos,":{$campo}");
        }
        $this->fields = implode(',',str_replace(":","",$campos) );
        $params = implode(',',$campos );
        $query = " INSERT INTO {$this->table} ({$this->fields})VALUES ({$params});"; 
        $pdo = $this->db->prepare($query);
        $pdo->execute($dados);
        return $this->db->lastInsertId();
    }
    public function update(array $dados)
    {
        $query = " UPDATE {$this->table} SET nome = :nome ;"; 
        $pdo = $this->db->prepare($query);
        $pdo->bindParam(":nome",$dados['nome'],\PDO::PARAM_STR);
        $pdo->execute();
        return $pdo->fetch(\PDO::FETCH_ASSOC);
    }
}
