<?php
namespace Config\Routes;

abstract class Routers
{
    private $routes;

    public function __construct()
    {
        $this->init();
        $this->run($this->getUrl());
    }
    
    abstract protected function init();
    
    protected function run($url)
    {
        array_walk($this->routes, function ($route) use ($url) {
            if ($url == $route['route']) {
                $class = "App\\Controllers\\" . ucfirst($route['controller']);
                $controller = new $class;
                $action = $route['action'];
                $controller->$action();
            }else {
                if( strpos($route['route'], ":id") ){
                    $keys = explode('/',$route['route']);
                    $values = explode('/',$url);
                    if(count($keys)==count($values)){
                        $array = array_combine($keys, $values);
                        $class = "App\\Controllers\\" . ucfirst($route['controller']);
                        $controller = new $class;
                        $action = $route['action'];
                        $controller->$action($array[':id']);
                    }
                    
                }
            }
        });
    }
    protected function setRoutes(array $routes)
    {
        $this->routes = $routes;
    }
    protected function getUrl()
    {
        return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    }
}
