<?php
namespace App\Models;

use Config\Database\Table;

class Pedido extends Table
{
    protected $table = "pedidos";
    protected $fields = ['id_cliente','parcelas','valor','forma_pagamento','ip'];
    protected $ip ;
    
    public function getIp(){
        $handle = curl_init('https://api.ipgeolocation.io/getip');  
        $opts = array(CURLOPT_RETURNTRANSFER =>true) ;
        curl_setopt_array($handle, $opts); 
        $data = curl_exec($handle);
        return json_decode($data)->ip;
    }
    public function getAll()
    {
        $query = " SELECT {$this->table}.* , clientes.nome as cliente,round((valor/parcelas),2) as vl_parcela
        FROM {$this->table} INNER JOIN clientes ON (id_cliente=clientes.id);";
        return $this->db->query($query)->fetchAll(\PDO::FETCH_OBJ);
    }
    public function find($id)
    {
        $query = " SELECT {$this->table}.* , clientes.nome as cliente,round((valor/parcelas),2) as vl_parcela
        FROM {$this->table} INNER JOIN clientes ON (id_cliente=clientes.id)  WHERE pedidos.id=:id;";
        $pdo = $this->db->prepare($query);
        $pdo->bindParam(":id",$id);
        $pdo->execute();
        return $pdo->fetch(\PDO::FETCH_OBJ);
    }
}