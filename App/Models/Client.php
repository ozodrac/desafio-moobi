<?php
namespace App\Models;

use Config\Database\Table;

class Client extends Table
{
    protected $table = "clientes";
    protected $fields = ['nome'];
    
}