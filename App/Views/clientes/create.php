<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Clientes
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class='fa fa-newspaper-o'></i> Criar novo Cliente
                            </div>

                            <div class="card-body">
                                <form action="/clientes/save" method="post">

                                    <div class="row">
                                        <div class="form-group col-sm-4">
                                            <label for="nome">Nome</label>
                                            <input class="form-control" stype="text" name="nome" id="nome">
                                            <span class="help-block text-danger"></span>
                                        </div>

                                    </div>
                                    <input class="btn btn-primary" type="submit" value="Criar">

                                    <a class="btn btn-danger" href="/clientes">Voltar</a>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>