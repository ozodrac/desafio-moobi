<?php
if(!empty($this->view->message)) : ?>
<div class="alert {$this->view->message_class} alert-info alert-dismissible fade show" role="alert">
  <?=$this->view->message;?>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<?php
endif;
?>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Clientes
            </div>
            <div class="card-body">
                <table class="table btn-sm" id="dataTable">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Nome</th>
                            <th>Nome</th>
                            <th>Nome</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($this->view->clientes as $cliente) : ?>
                            <tr>
                                <td><?= $cliente->id; ?></td>
                                <td><?= $cliente->nome; ?></td>
                                <td><?= $cliente->nome; ?></td>
                                <td><?= $cliente->nome; ?></td>
                                <td><?= $cliente->nome; ?></td>
                            </tr>
                        <?php
                        endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Nome</th>
                            <th>Nome</th>
                            <th>Nome</th>
                            <th>Nome</th>
                            <th>Ações</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>