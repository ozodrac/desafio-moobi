<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Pedidos
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class='fa fa-newspaper-o'></i> Vizualizar Pedido
                            </div>

                            <div class="card-body">
                                <table class="table btn-sm"  width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Cliente</th>
                                            <th class="text-center">Parcelas</th>
                                            <th class="text-center">Vl Parcela</th>
                                            <th class="text-center">Vl. Total</th>
                                            <th class="text-center">Pagamento</th>
                                            <th class="text-center">ip</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            <tr>
                                                <td class="text-center"><?= $this->view->pedido->id; ?></td>
                                                <td class="text-center"><?= $this->view->pedido->cliente; ?></td>
                                                <td class="text-center"><?= $this->view->pedido->parcelas; ?></td>
                                                <td class="text-center"><?= $this->view->pedido->vl_parcela; ?></td>
                                                <td class="text-center"><?= $this->view->pedido->valor; ?></td>
                                                <td class="text-center"><?= $this->view->pedido->forma_pagamento; ?></td>
                                                <td class="text-center"><?= $this->view->pedido->ip; ?></td>
                                            </tr>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <?php
                                    for($i=1;$i<=$this->view->pedido->parcelas;$i++):?>
                                        <!-- Earnings (Monthly) Card Example -->
                                        <div class="col-xl-3 col-md-6 mb-4">
                                            <div class="card border-left-success shadow h-100 py-2">
                                                <div class="card-body">
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col mr-2">
                                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><?="Parcela {$i}/{$this->view->pedido->parcelas}"?></div>
                                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->view->pedido->vl_parcela?></div>
                                                    </div>
                                                    <div class="col-auto">
                                                    <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                    endfor;
                                    ?>
                                </div>
                                <a class="btn btn-danger" href="/pedidos">Voltar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>