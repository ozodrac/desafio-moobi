<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Pedidos
            </div>
            <div class="card-body table-responsive">
                <table class="table btn-sm" id="dataTable"  width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Cliente</th>
                            <th class="text-center">Parcelas</th>
                            <th class="text-center">Vl Parcela</th>
                            <th class="text-center">Vl. Total</th>
                            <th class="text-center">Pagamento</th>
                            <th class="text-center">ip</th>
                            <th class="text-center">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($this->view->pedidos as $pedido) : ?>
                            <tr>
                                <td class="text-center"><?= $pedido->id; ?></td>
                                <td class="text-center"><?= $pedido->cliente; ?></td>
                                <td class="text-center"><?= $pedido->parcelas; ?></td>
                                <td class="text-center"><?= $pedido->vl_parcela; ?></td>
                                <td class="text-center"><?= $pedido->valor; ?></td>
                                <td class="text-center"><?= $pedido->forma_pagamento; ?></td>
                                <td class="text-center"><?= $pedido->ip; ?></td>
                                <td class="text-center">
                                    <a href="/pedidos/1" class="btn btn-info btn-circle btn-sm" title="Ver Pedido"><i class="fas fa-info-circle"></i></a>
                                    <a href="/pedidos/delete/<?= $pedido->id; ?>" class="btn btn-danger btn-circle btn-sm" title="Deletar"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php
                        endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Cliente</th>
                            <th>parcelas</th>
                            <th>valor total</th>
                            <th>forma_pagamento</th>
                            <th>ip</th>
                            <th>Ações</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
</div>
