<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Pedidos
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class='fa fa-newspaper-o'></i> Criar novo Pedido
                            </div>

                            <div class="card-body">
                                <form action="/pedidos/save" method="post">

                                    <div class="row">
                                        <div class="form-group col-sm-4">
                                            <label for="nome">Cliente</label>
                                            <select class="form-control" name="id_cliente">
                                                <?php
                                                    if(!empty($this->view->clientes)):
                                                        foreach($this->view->clientes as $cliente):?>
                                                        <option value="<?=$cliente->id?>"><?=$cliente->nome?></option>
                                                        <?php
                                                        endforeach;
                                                    endif;
                                                ?>
                                                <option></option>
                                            </select>
                                            <span class="help-block text-danger"></span>
                                        </div>
                                       
                                        <div class="form-group col-sm-2">
                                            <label for="nome">Valor do Pedido</label>
                                            <input class="form-control" type="text" name="valor">
                                            <span class="help-block text-danger"></span>
                                        </div>
                                        <div class="form-group col-sm-2">
                                            <label for="nome">Nº Parcelas</label>
                                            <input class="form-control" type="text" name="parcelas">
                                            <span class="help-block text-danger"></span>
                                        </div>
                                        <div class="form-group col-sm-2">
                                            <label for="nome">Pagamento</label>
                                            <select class="form-control" name="forma_pagamento">
                                                <option value="CC">Crédito</option>
                                                <option value="CD">Débito</option>
                                                <option value="V">à Vista</option>
                                            </select>
                                            <span class="help-block text-danger"></span>
                                        </div>
                                    </div>
                                    <input class="btn btn-primary" type="submit" value="Criar">

                                    <a class="btn btn-danger" href="/pedidos">Voltar</a>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>