<?php

namespace App\Controllers;

use Config\Controllers\CoreController;

class Home extends CoreController
{
    public function index(){
        $this->render("index");
    }
}
