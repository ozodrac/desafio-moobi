<?php
namespace App\Controllers;
use Config\Controllers\CoreController;
use Config\Database\Database;
use App\Models\Pedido;
use App\Models\Client;

class Pedidos extends CoreController
{
     public function index()
    {
        $pedido = new Pedido(Database::getDb());
        $this->view->pedidos = $pedido->getAll();
        $this->render("index");
    }

    public function create()
    {
        $cliente = new Client(Database::getDb());
        $this->view->clientes = $cliente->getAll();
        $this->render("create");
    }
    public function edit($id)
    {
        $pedido = new Pedido(Database::getDb());
        $this->view->pedido = $pedido->find($id);
        $this->render("edit");
    }
    public function store()
    {
        $pedido = new Pedido(Database::getDb());
        $_POST['ip'] = $pedido->getIp();

        switch ($_POST['forma_pagamento']) {
            case 'CC':
                $_POST['valor'] = ($_POST['valor']*1.03);
            break;
            case 'CD':
                $_POST['valor'] -= ($_POST['valor']*0.03);
            break;
            case 'V':
                $_POST['valor'] -= ($_POST['valor']*0.1);
            break;
            
            default:
                $_POST['valor'] = $_POST['valor'];
            break;
        }
        if($pedido->store($_POST)){
            header("Location: /pedidos");
            $this->index();
        }
    }
    public function delete($id)
    {
        $pedido = new Pedido(Database::getDb());
        $this->view->pedido = $pedido->delete($id);
        header("Location: /pedidos");
    }
}
