<?php

namespace App\Controllers;

use Config\Controllers\CoreController;
use Config\Database\Database;
use App\Models\Client;

class Clientes extends CoreController
{
    
    public function index()
    {
        $cliente = new Client(Database::getDb());
        $this->view->clientes = $cliente->getAll();
        $this->render("index");
    }

    public function create()
    {
        $this->render("create");
    }
    public function edit()
    {
        $this->render("edit");
    }
    public function store()
    {
        $cliente = new Client(Database::getDb());
        if($cliente->store($_POST)){
            header("Location: /clientes");
            $this->index();
        }
    }
    public function delete($id)
    {
    }
}
