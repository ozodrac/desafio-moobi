# desafio-moobi >Entrega dia 18/05/2019 11h


### 1. Dados de entrada: 
* [x]  Valor total do pedido
* [x]  Número de parcelas;
* [x]  Nome Cliente;
* [x]  Forma de Pagamento(credito,debito,a vista);
* [x]  Dados da Localizacao do Ip do pedido ( pegar ip automaticamento/ ipgeolocation.io)
### 2. Dados de Saida:
* [x]  Exibir Pedido;
* [x]  Exibir Parcelas do Pedido;
### 3. Regras especificas das formas de pagamento:
* [x]  Cartão de Crédito tem acréssimo de 3%
* [x]  Débito tem desconto de 3%
* [x]  À vista tem desconto de 10%

## OBS: Nãao deverá permitir a edição do pedido, apenas remoção


## Requisitos:

* [x]  Escrito em PHP;
* [x]  Sem Frameworks;
* [x]  Utilizar Git em repositório publico;
* [x]  Usar banco de dados Mysql (criar e modelar estrutura);

# Extras:
 
* [x]  Orientação Objetos
* [ ]  Estrutura dos commits;
* [x]  Código Limpo e facil Manutenção;
* [ ]  SOLID;
* [ ]  Testes Unitários;
* [x]  Padrões de Projetos utilizados corretamente;



|Letra   | Sigla  | Nome   | Definição|
| ------ | ------ | ------ | ------   |
| S      | SRP    |Principio da Responsabilidade Única |Uma classe deve ter um, e somente um, motivo para mudar.                        |
| O      | OCP    |Princípio Aberto-Fechado            |Você deve ser capaz de estender um comportamento de uma classe, sem modificá-lo.|
| L      | LSP    |Princípio da Substituição de Liskov |As classes base devem ser substituíveis por suas classes derivadas.             |
| I      | ISP    |Princípio da Segregação da Interface|Muitas interfaces específicas são melhores do que uma interface única.          |
| D      | DIP    |Princípio da inversão da dependência|Dependa de uma abstração e não de uma implementação.                            |




php -S localhost:8080